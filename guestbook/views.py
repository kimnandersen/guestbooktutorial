from django.shortcuts import render

from .models import Entry
from .forms import EntryForm

# Create your views here.
def index(request):
	entries = Entry.objects.order_by('-date_added')
	context = {'entries': entries}

	return render(request, 'guestbook/index.html', context)

def sign(request):

	if request.method == 'POST':
		form = EntryForm(request.POST)
		
		if form.is_valid():
			new_entry = Entry(author=request.POST['author'], entry=request.POST['entry'])
			new_entry.save();

			return render(request, 'guestbook/thanks.html')
		else:
			return render(request, 'guestbook/sign.html', {'form': EntryForm(), 'message': 'Invalid input.'})
	else:
		form = EntryForm()

		context = {'form': form, 'message': ''}
		return render(request, 'guestbook/sign.html', context)