from django.db import models

from django.utils import timezone

# Create your models here.
class Entry(models.Model):
	author = models.CharField(max_length=30)
	entry = models.TextField()
	date_added = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return '<Author: {}, ID: {}, Added at: {} UTC>'.format(self.author, self.id, self.date_added)
