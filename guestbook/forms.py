from django import forms

class EntryForm(forms.Form):
	author = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Name'}))
	entry = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '10', 'cols': '60', 'placeholder': 'Enter your Entry'}))

